interface Switch {
    var device: Device
    fun turnOn()
    
}

interface Device {
    fun run()
    
}

class RemoteControl(override var device: Device) : Switch {
    override fun turnOn() = device.run()
   
}

class TV : Device {
    override fun run() = println("TV turned on")
    
}
class Radio : Device {
    override fun run() = println("Radio turned on")
    
}
fun main(args: Array<String>) {
    var tvRemoteControl = RemoteControl(device = TV())
    tvRemoteControl.turnOn()
   
    var radioRemoteControl = RemoteControl(device = Radio())
    radioRemoteControl.turnOn()
    
}


// tomado de: https://chercher.tech/kotlin/bridge-design-pattern-kotlin